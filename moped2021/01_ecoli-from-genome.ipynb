{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# E.coli reconstruction\n",
    "\n",
    "To construct a metabolic model for E. coli K-12, we first import the MetaCyc database into moped from its PGDB files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1.9.0\n"
     ]
    }
   ],
   "source": [
    "import pkg_resources\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "from pathlib import Path\n",
    "from moped import Reaction, Model\n",
    "from moped.utils import blast_is_installed\n",
    "from cobra.flux_analysis import pfba\n",
    "print(pkg_resources.get_distribution(\"moped\").version)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Model: Model\n",
       "    compounds: 12840\n",
       "    reactions: 18393"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "metacyc = Model()\n",
    "metacyc.read_from_pgdb(pgdb_path=Path(\"..\") / \"data\" / \"metacyc\" / \"25.1\" / \"data\")\n",
    "metacyc\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create model from proteome/genome or database\n",
    "\n",
    "If we have BLAST installed, we can use the functions `create_submodel_from_proteome` or `create_submodel_from_genome` to import a proteome/genome fasta file and BLAST it against the MetaCyc database to construct a model of all reactions that can be found in the genome sequence.\n",
    "\n",
    "Alternatively, its possible to import the BioCyc PGDB files of E.coli K-12 using `read_from_pgdb` and `update_from_reference` to avoid version discrepancies."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Model: ecoli\n",
       "    compounds: 2182\n",
       "    reactions: 2741"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "if not blast_is_installed():\n",
    "    raise OSError(\"Could not find blast. Please make sure its on your path.\")\n",
    "\n",
    "fastacyc = metacyc.create_submodel_from_proteome(\n",
    "    \"../data/proteomes/ecoli.fasta\",\n",
    "    name=\"ecoli\",\n",
    "    cache_blast_results=True,\n",
    ")\n",
    "fastacyc"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Topological gapfilling\n",
    "\n",
    "In order to apply topological gapfilling to enable our model to produce biomass compounds, we apply cofactor and reversibility duplication for both draft and reference objects (see manuscript).\n",
    "\n",
    "After getting a the template biomass compounds using `get_biomass_template`, we define a minimal medium seed including weak mock cofactors.\n",
    "\n",
    "The function `get_gapfilling_reactions` uses Meneco gapfilling to find a minimal set of missing reactions from the reference moped object to produce all targets, and returns the list of reaction IDs in a list.\n",
    "A result of previous gapfilling is provided, but can be commented out for new calculations.\n",
    "\n",
    "These reactions are then added to the draft moped object from the reference moped object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "medium = [\n",
    "    \"ALPHA-GLUCOSE_c\",\n",
    "    \"WATER_c\",\n",
    "    \"PROTON_c\",\n",
    "    \"OXYGEN-MOLECULE_c\",\n",
    "    \"Pi_c\",\n",
    "    \"SULFATE_c\",\n",
    "    \"AMMONIA_c\",\n",
    "]\n",
    "\n",
    "seed = medium + metacyc.get_weak_cofactor_duplications()\n",
    "\n",
    "biomass_comp = fastacyc.get_biomass_template()\n",
    "\n",
    "# Manual additions to be always sure they are there\n",
    "for rxn in metacyc.pathways[\"GLYCOLYSIS\"]:\n",
    "    if rxn not in fastacyc.reactions:\n",
    "        fastacyc.add_reaction_from_reference(metacyc, rxn)\n",
    "\n",
    "for rxn in metacyc.pathways[\"TCA\"]:\n",
    "    if rxn not in fastacyc.reactions:\n",
    "        fastacyc.add_reaction_from_reference(metacyc, rxn)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "metacyc.cofactor_duplication()\n",
    "metacyc.reversibility_duplication()\n",
    "fastacyc.cofactor_duplication()\n",
    "fastacyc.reversibility_duplication()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Topological gapfilling can take quite a while, so we cached the results here\n",
    "\n",
    "gapfill_result = [\n",
    "    \"AMONITRO-RXN_c__cof__\",\n",
    "    \"RXN-16318_c__cof__\",\n",
    "    \"RXN-20683__var__1_c__cof__\",\n",
    "    \"OROTATE-REDUCTASE-NADH-RXN_c__cof__\",\n",
    "    \"CYTOCHROME-C-OXIDASE-RXN_c_p__cof__\",\n",
    "]\n",
    "if gapfill_result is None:\n",
    "    gapfill_result = fastacyc.get_gapfilling_reactions(\n",
    "        reference_model=metacyc,\n",
    "        seed=seed,\n",
    "        targets=[\"UTP_c\", \"ATP_c\", \"GTP_c\", \"CTP_c\"],\n",
    "        verbose=True,\n",
    "    )\n",
    "for reaction_id in gapfill_result:\n",
    "    fastacyc.add_reaction_from_reference(reference_model=metacyc, reaction_id=reaction_id)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['AMONITRO-RXN_c__cof__',\n",
       " 'RXN-16318_c__cof__',\n",
       " 'RXN-20683__var__1_c__cof__',\n",
       " 'OROTATE-REDUCTASE-NADH-RXN_c__cof__',\n",
       " 'CYTOCHROME-C-OXIDASE-RXN_c_p__cof__']"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "gapfill_result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Topological gapfilling can take quite a while, so we cached the results here\n",
    "gapfill_result = [\n",
    "    \"ASPDECARBOX-RXN_c\",\n",
    "    \"ORNITHINE-CYCLODEAMINASE-RXN_c\",\n",
    "    \"ARGININE-DEIMINASE-RXN_c__rev__\",\n",
    "    \"ACETYL-COA-CARBOXYLTRANSFER-RXN_c\",\n",
    "    \"RXN-18378_c\",\n",
    "]\n",
    "if gapfill_result is None:\n",
    "    gapfill_result = fastacyc.get_gapfilling_reactions(\n",
    "        reference_model=metacyc,\n",
    "        seed=seed,\n",
    "        targets=biomass_comp,\n",
    "        verbose=True,\n",
    "    )\n",
    "for reaction_id in gapfill_result:\n",
    "    fastacyc.add_reaction_from_reference(reference_model=metacyc, reaction_id=reaction_id)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['ASPDECARBOX-RXN_c',\n",
       " 'ORNITHINE-CYCLODEAMINASE-RXN_c',\n",
       " 'ARGININE-DEIMINASE-RXN_c__rev__',\n",
       " 'ACETYL-COA-CARBOXYLTRANSFER-RXN_c',\n",
       " 'RXN-18378_c']"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "gapfill_result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Remove duplications\n",
    "fastacyc.remove_cofactor_duplication()\n",
    "fastacyc.remove_reversibility_duplication()\n",
    "metacyc.remove_cofactor_duplication()\n",
    "metacyc.remove_reversibility_duplication()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Flux Balance Analysis (FBA)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To test our model using FBA later, we define a biomass reaction, which is a simplified version of the E. coli iJO1366 reaction, and an ATPase reaction as an atp maintenance requirement, and add both to the draft moped object.\n",
    "\n",
    "Sometimes not all Biomass compounds are included in draft moped objects. To avoid problems, we also add any missing compounds from to the draft object from the reference object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "atpase_rxn = Reaction(\n",
    "    id=\"ATPase\",\n",
    "    stoichiometries={\"ATP_c\": -1, \"WATER_c\": -1, \"ADP_c\": 1, \"Pi_c\": 1, \"PROTON_c\": 1},\n",
    "    bounds=(0.0, 1000.0),\n",
    ")\n",
    "biomass_rxn = Reaction(\n",
    "    id=\"BIOMASS\",\n",
    "    stoichiometries=biomass_comp,\n",
    "    bounds=(0.0, 1000.0),\n",
    ")\n",
    "\n",
    "excluded = set(medium + [\"CARBON-DIOXIDE_c\"])\n",
    "cytosol = set(fastacyc.get_compounds_of_compartment(\"CYTOSOL\")).difference(excluded)\n",
    "\n",
    "for compound_id in biomass_comp:\n",
    "    if compound_id not in fastacyc.compounds:\n",
    "        fastacyc.add_compound_from_reference(reference_model=metacyc, compound_id=compound_id)\n",
    "\n",
    "for compound_id in excluded:\n",
    "    fastacyc.add_transport_reaction(compound_id=compound_id, compartment_id=\"EXTRACELLULAR\")\n",
    "    fastacyc.add_medium_component(compound_id=compound_id, extracellular_compartment_id=\"EXTRACELLULAR\")\n",
    "\n",
    "\n",
    "fastacyc.add_reaction(biomass_rxn)\n",
    "fastacyc.add_reaction(atpase_rxn)\n",
    "\n",
    "fastacyc.reactions[\"EX_ALPHA-GLUCOSE_e\"].bounds = (-10, 10)\n",
    "fastacyc.objective = {\"BIOMASS\": 1}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to apply Flux Balance Analysis to our model, we add exchange reactions that can introduce compounds into our model for all medium seed compounds using `add_transport_reaction` and `add_medium_component`.  \n",
    "In this model, we allow Carbon Dioxide to leave the model via a respective efflux reaction.\n",
    "We define a new variable in which we export our moped object into a CobraPy object, and set the objective coefficient of our Biomass reaction to 1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Scaling...\n",
      " A: min|aij| =  1.000e+00  max|aij| =  1.000e+00  ratio =  1.000e+00\n",
      "Problem data seem to be well scaled\n"
     ]
    }
   ],
   "source": [
    "all_exporters = fastacyc.copy()\n",
    "\n",
    "for compound_id in cytosol:\n",
    "    all_exporters.add_transport_reaction(compound_id, \"EXTRACELLULAR\")\n",
    "    all_exporters.add_efflux(compound_id, \"EXTRACELLULAR\")\n",
    "\n",
    "m_ex = all_exporters.to_cobra()\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pfba(m_ex).to_frame()\n",
    "ex = df[df.index.str.startswith(\"EX\")]\n",
    "exporters = ex[ex[\"fluxes\"].abs() > 1e-3]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "new_to_export = {cpd for i in exporters.index if (cpd := f\"{i[3:-2]}_c\") not in excluded}\n",
    "\n",
    "for compound_id in new_to_export:\n",
    "    fastacyc.add_transport_reaction(compound_id=compound_id, compartment_id=\"EXTRACELLULAR\")\n",
    "    fastacyc.add_medium_component(compound_id=compound_id, extracellular_compartment_id=\"EXTRACELLULAR\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "1.163510384574661"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "m_fc = fastacyc.to_cobra()\n",
    "m_fc.optimize()[\"BIOMASS\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## ATP maintenance scan\n",
    "\n",
    "Good indicator for thermodynamically infeasible loops"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAYAAAAD4CAYAAADlwTGnAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjUuMSwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy/YYfK9AAAACXBIWXMAAAsTAAALEwEAmpwYAAApZklEQVR4nO3dd3hUZfr/8fedRu8JRaoURToYOiSodBWQRUVcYW3YkJJ117LFvuW7biiKNEXAVVBBlEUUgmJClyAdpIOACJEq0uH5/TEHf9lICTLJZGY+r+uaK5nnzDm5Hw35ZM45uR9zziEiIuEnItAFiIhIYCgARETClAJARCRMKQBERMKUAkBEJExFBbqAyxEbG+uqVKkS6DJERILK0qVLf3DOxWUdD6oAqFKlCunp6YEuQ0QkqJjZ9vON6xSQiEiYUgCIiIQpBYCISJhSAIiIhCkFgIhImFIAiIiEKQWAiEiYCosAWLRlH2/O28qZs2p9LSJyTlgEwCcrd/Pi9LX0GLmAjXt+DHQ5IiJ5QlgEwAtdazPkzgZs++Enbh42j2Gfb+Tk6bOBLktEJKAuGQBmNtbM9prZ6gtsr2lmC83shJk9kWXbNjNbZWbLzSw903hJM0sxs43exxJXPpWLzoFuDcuTkpRIhzplSU7ZQJfX5rFy58Gc/LIiInladt4BjAM6XmT7fqA/8MoFtt/gnGvgnIvPNPYU8Llzrgbwufc8x8UWzserdzVkTO94Dhw9Sbfh8/n7jHUcO3kmN768iEiecskAcM6l4fshf6Hte51zS4BTl/F1uwLjvc/HA90uY98r1q5WGWYNSuTOxhUZlbaFTkPTWLRlX26WICIScDl9DcABs8xsqZn1zTRexjm32/v8e6DMhQ5gZn3NLN3M0jMyMvxWWLEC0fy9ez3efaApZx30HL2IP01dxY/HLyfHRESCV04HQCvnXCOgE/CYmSVkfYFzzuELivNyzo12zsU75+Lj4n7RzvqKtagey8yBCTzQ6momfvUt7Qen8cU3e/z+dURE8pocDQDn3C7v415gKtDE27THzMoBeB/35mQdl1IgJpI/31KLKY+0oEj+KO4bl87AScvY/9PJQJYlIpKjciwAzKyQmRU59znQHjh3J9E0oI/3eR/g45yq43I0rFSC6Y+3ZsBNNfhk1W7aJqcybcV3+N6kiIiEFrvUDzczmwi0AWKBPcCzQDSAc26kmZUF0oGiwFngCFDLe/1U7zBRwLvOuZe9Y5YC3gcqAduBO5xzF7zQfE58fLzLrRXBvvn+ME9OXsmKnYdoe10ZXupWh7LF8ufK1xYR8SczW5rlTkzfeDD9dpubAQBw5qzjrflbeWXWeqIjInjm5uvo2bgiZpZrNYiIXKkLBUBY/CXwrxUZYTzQuiozByZQp3wxnv5wFb3GLGb7vp8CXZqIyBVTAGRD5VKFePfBpvy9e11W7zpEhyFpvDF3i5rLiUhQUwBkk5lxV5NKpCQl0qp6LC99so7uIxaw/ns1lxOR4KQAuExli+VnTO94ht3VkB37j3LLq3MZMnuDmsuJSNBRAPwKZkaX+lcxOymRm+uWY8jsjdz66jyW7zgY6NJERLJNAXAFShaKYUjPhoz9XTyHj5+i++vzeWn6WjWXE5GgoADwgxtrlmHWoATualKJN+ZtpcOQNBZs/iHQZYmIXJQCwE+K5I/m5dvqMqlvMyIMeo1ZzNMfruSwmsuJSB6lAPCzZlVL8dnABB5KrMp7S3bQLjmV2WvVXE5E8h4FQA7IHx3J052u46PHWlKiYAwPTEjn8YnL2HfkRKBLExH5mQIgB9WrUJxp/Vrx+3bXMHP197RNTuWjZbvUXE5E8gQFQA6LiYrg8Ztq8En/VlSJLcTA95Zz//h0vjt4LNCliUiYUwDkkhplijD54Rb89ZZaLNy8j/aD0/jPou2cVTsJEQkQBUAuioww7mt1NbMGJdCgYnH+/NFq7hqziK0/qLmciOQ+BUAAVCxZkLfvb8L//aYea3cfpuOQNEalbub0GbWTEJHcowAIEDPjjsYVmZ2USOI1cfz902/oPmIB63YfDnRpIhImLhkAZjbWzPaa2eoLbK9pZgvN7ISZPZFpvKKZzTGztWa2xswGZNr2nJntMrPl3qOzf6YTfMoUzc+oe65neK9GfHfwGLe+Oo/kWes5cVrtJEQkZ2XnHcA4oONFtu8H+gOvZBk/DfzeOVcLaAY8Zma1Mm0f7Jxr4D1mXEbNIcfMuLleOVIGJdKlwVUM+2ITNw+bx9LtBwJdmoiEsEsGgHMuDd8P+Qtt3+ucWwKcyjK+2zn3tff5j8A6oPyVlRvaShSKIfmOBoy7tzHHTp6hx8gFPP/fNRw9eTrQpYlICMqVawBmVgVoCCzONNzPzFZ6p5hKXGTfvmaWbmbpGRkZOV1qntDm2tLMHJTAPc0q89b8bbQfnMa8jWouJyL+leMBYGaFgSnAQOfcuSucI4BqQANgN/DvC+3vnBvtnIt3zsXHxcXldLl5RuF8UbzQtQ7vP9ScmMgIfvvmYv44eQWHjqm5nIj4R44GgJlF4/vh/45z7sNz4865Pc65M865s8AYoElO1hHMmlxdkhkDWvNIm2pM+XoX7ZJTmbnm+0CXJSIhIMcCwMwMeBNY55xLzrKtXKantwHnvcNIfPJHR/Jkx5p8/FhLYgvn46G3l/LYO1+T8aOay4nIr2eXakxmZhOBNkAssAd4FogGcM6NNLOyQDpQFDgLHAFqAfWAucAqbxzgGefcDDN7G9/pHwdsAx5yzu2+VLHx8fEuPT39siYYak6dOcvotC0Mnb2RAjGR/PWWWnRvVB5f3oqI/JKZLXXOxf9iPJg6UyoA/r9Ne4/w5JSVLN1+gMRr4vhb97qUL14g0GWJSB50oQDQXwIHqeqlC/PBQ815vkttlmzbT/vkVCYs3KbmciKSbQqAIBYRYfRpUYWZAxNoVLkEf/14DXeOXsjmjCOBLk1EgoACIARULFmQCfc14ZXb67NhzxE6DZ3L619u4pSay4nIRSgAQoSZ0eP6CqQkJXBTzdL832fr6TZ8Pqt3HQp0aSKSRykAQkzpIvkZ8dvrGXF3I/YcPkHX4fP518xvOH5KzeVE5H8pAEJUp7rlmJ2UwG0NyzN8zmY6D5tL+rYLtnQSkTCkAAhhxQvG8Mrt9ZlwXxNOnDrL7aMW8ty0Nfx0Qs3lREQBEBYSrolj1qAE+jSvwviFvuZyqRvCo7GeiFyYAiBMFMoXxXNdajP54ebkj46gz9iv+P37Kzh49GSgSxORAFEAhJnrK5fkk/6t6XdDdT5avou2yWl8uuqSXThEJAQpAMJQ/uhInuhwLdP6taRM0Xw88s7XPPz2UvYePh7o0kQkFykAwljtq4rx8WMtebJjTb5Yv5e2yal8kL6DYOoPJSK/ngIgzEVFRvBIm2p8OqA1NcsW5Q+TV9J77Ffs2H800KWJSA5TAAgA1eIKM6lvM17sWpuvtx+gw5A03pq/lTNqLicSshQA8rOICOOe5lWYlZRI4yolef6/a7lj1EI27f0x0KWJSA5QAMgvlC9egHH3Nib5jvpszjhC56HzeO2LjWouJxJishUAZjbWzPaa2XmXbjSzmma20MxOmNkTWbZ1NLP1ZrbJzJ7KNH61mS32xt8zs5grm4r4k5nRvVEFUgYl0q52GV6ZtYEur6m5nEgoye47gHFAx4ts3w/0B17JPGhmkcBwoBO+ZSLvMrNa3uZ/AoOdc9WBA8D92S9bcktckXwM79WIUfdcz74jvuZy//hUzeVEQkG2AsA5l4bvh/yFtu91zi0BTmXZ1ATY5Jzb4pw7CUwCunoLxt8ITPZeNx7odpm1Sy7qULssKUmJ9GhUgZGpm+k0dC6Lt+wLdFkicgVy+hpAeWBHpuc7vbFSwEHn3Oks479gZn3NLN3M0jMy1L8mkIoViOafPerxzgNNOX32LHeOXsRfPlrNj8ez5r6IBIM8fxHYOTfaORfvnIuPi4sLdDkCtKwey8yBCdzX8mr+s3g7HQanMWf93kCXJSKXKacDYBdQMdPzCt7YPqC4mUVlGZcgUTAmir/eWospj7SgUL4o7n1rCUnvLefAT2ouJxIscjoAlgA1vDt+YoCewDTn6zUwB+jhva4P8HEO1yI5oFGlEkzv34r+N9Vg2orvaJucyvSV36mdhEgQsOz8QzWziUAbIBbYAzwLRAM450aaWVkgHSgKnAWOALWcc4fNrDMwBIgExjrnXvaOWRXfReGSwDLgt865ExerIz4+3qWnp1/+LCVXrNt9mCenrGTlzkO0q1WGl7rVoUzR/IEuSyTsmdlS51z8L8aD6Tc1BUDed/rMWcbO38q/Z20gJiqCP998HXfEV8R345eIBMKFAiDPXwSW4BIVGUHfhGrMHJhArXJFeXLKKu5+YzHf7lNzOZG8RgEgOaJKbCEmPtiMl2+rw8qdh+gwJI0356m5nEheogCQHBMRYdzdtDIpSQk0r1aKF6ev5TcjFrBhj5rLieQFCgDJceWKFeDNPvEM7dmAb/cf5eZhcxk6eyMnT6u5nEggKQAkV5gZXRuUJ2VQAp3qlGPw7A3c+uo8Vuw4GOjSRMKWAkByVanC+Rh2V0Pe6B3PoWOnuO31+fxtxjqOnVRzOZHcpgCQgGhbqwyzkhK4s3ElRqdtodPQNBZuVnM5kdykAJCAKZo/mr93r8u7DzbFAXeNWcQzU1dxWM3lRHKFAkACrkW1WD4bkEDfhKpM+upb2ien8fm6PYEuSyTkKQAkTygQE8kzna/jw0dbUqxANPePT6f/xGXsO3LR7iAicgUUAJKnNKhYnP8+3opBba/h09W7aTc4jY+X71JzOZEcoACQPCcmKoIBbWsw/fHWVCxZkAGTlvPA+HR2HzoW6NJEQooCQPKsa8sW4cNHWvDnm69j/uYfaJ+cxruLv+Ws2kmI+IUCQPK0yAjjgdZVmTkwgTrli/HM1FX0emMR2374KdCliQQ9BYAEhcqlCvHug035R/e6rNl1mI5D0xiTtkXN5USugAJAgoaZ0bNJJVKSEmlVPZaXZ6yj++vz+eb7w4EuTSQoXTIAzGysme01s9UX2G5mNszMNpnZSjNr5I3fYGbLMz2Om1k3b9s4M9uaaVsDf05KQlvZYvkZ0zueV+9qyM4Dx7hl2DySUzZw4rTaSYhcjuy8AxgHdLzI9k5ADe/RFxgB4Jyb45xr4JxrANwIHAVmZdrvD+e2O+eWX37pEs7MjFvrX0VKUiK31CvHsM83cuur81j27YFAlyYSNC4ZAM65NGD/RV7SFZjgfBYBxc2sXJbX9AA+dc5pWSjxq5KFYhjSsyFjfxfPj8dP033EAl6cvpajJ08HujSRPM8f1wDKAzsyPd/pjWXWE5iYZexl75TRYDPLd6GDm1lfM0s3s/SMjAw/lCuh6MaaZZg1KIG7m1bizXlb6ThkLgs2/RDoskTytBy/COy9G6gLzMw0/DRQE2gMlASevND+zrnRzrl451x8XFxcjtYqwa1I/mhe6laXSX2bEWHQ643FPDVlJYeOqbmcyPn4IwB2ARUzPa/gjZ1zBzDVOffzv0Ln3G7vlNEJ4C2giR/qEAGgWdVSfDYwgYcSq/J++g7aD04lZa2ay4lk5Y8AmAb09u4GagYccs7tzrT9LrKc/jl3jcDMDOgGnPcOI5FfK390JE93uo6PHmtJiYIxPDghnX7vfs0Pai4n8rOoS73AzCYCbYBYM9sJPAtEAzjnRgIzgM7AJnx3+tybad8q+N4dpGY57DtmFgcYsBx4+MqmIXJ+9SoUZ1q/VoxK3cyrX2xi3qYfePbWWnRrUB7f7x8i4cuCqctifHy8S09PD3QZEqQ27vmRP05ZybJvD3LDtXG8fFtdripeINBlieQ4M1vqnIvPOq6/BJawUaNMESY/3IK/3lKLRVv2035wGm8v2q7mchK2FAASViIjjPtaXc3MgQnUr1iMv3y0mp5jFrFVzeUkDCkAJCxVKlWQ/9zflH/+pi7rdh+m45A0RqZu5vSZs4EuTSTXKAAkbJkZdzauxOykRBKvieMfn37Dba8vYO13ai4n4UEBIGGvTNH8jLrneob3asTuQ8fo8to8/j1rvZrLSchTAIjgezdwc71ypAxKpEuDq3j1i03cPGweS7eruZyELgWASCYlCsWQfEcD3rq3MUdPnKbHyAU8/981/HRCzeUk9CgARM7jhmtLMyspkXuaVeat+dvoMCSNuRvVjFBCiwJA5AIK54viha51eP+h5sRERnDPm1/xx8krOHRUzeUkNCgARC6hydUlmTGgNY+0qcaUr3fRdnAqn63+PtBliVwxBYBINuSPjuTJjjX5+LGWxBXOx8P/Wcpj73xNxo9qLifBSwEgchnqlC/Gx/1a8ocO15Kydg9tk1OZsnQnwdRTS+QcBYDIZYqOjOCxG6ozY0BrqpcuzO8/WMHv3lrCroPHAl2ayGVRAIj8StVLF+aDh5rzfJfaLNm2n/bJqUxYuE3N5SRoKABErkBEhNGnRRVmDkygUeUS/PXjNdw5eiGbM44EujSRS8pWAJjZWDPba2bnXbnLWw1smJlt8hZ6b5Rp2xkzW+49pmUav9rMFnv7vGdmMVc+HZHAqFiyIBPua8Irt9dnw54jdBo6l9e/3MQpNZeTPCy77wDGAR0vsr0TUMN79AVGZNp2zDnXwHt0yTT+T2Cwc646cAC4P9tVi+RBZkaP6yuQkpTATTVL83+frafb8Pms3nUo0KWJnFe2AsA5lwbsv8hLugITvIXeFwHFz637ez7eWsA3ApO9ofH41gYWCXqli+RnxG+vZ8Tdjdhz+ARdh8/nXzO/4fgpNZeTvMVf1wDKAzsyPd/pjQHkN7N0M1tkZt28sVLAQefc6fO8XiQkdKpbjs+TEunesDzD52ym87C5pG+72O9RIrkrNy4CV/bWouwFDDGzapezs5n19QIkPSNDvVgkuBQrGM2/bq/PhPuacOLUWW4ftZDnpqm5nOQN/gqAXUDFTM8reGM458593AJ8CTQE9uE7TRSV9fVZOedGO+finXPxcXFxfipXJHclXBPHrEEJ9GlehfELt9F+cBppG/QLjQSWvwJgGtDbuxuoGXDIObfbzEqYWT4AM4sFWgJrne/PJucAPbz9+wAf+6kWkTypUL4onutSm8kPNyd/dAS9x37FEx+s4ODRk4EuTcKUZedP2M1sItAGiAX2AM8C0QDOuZHeRd3X8N0pdBS41zmXbmYtgFHAWXxhM8Q596Z3zKrAJKAksAz4rXPuoo1V4uPjXXp6+q+YpkjecvzUGV77YhMjUzdTvGAML3atTae6F7xvQuSKmNlS71T8/44HUw8TBYCEmjXfHeLJKStZveswHWuX5YWutSldNH+gy5IQc6EA0F8CiwRQ7auK8dGjLXmyY02+WL+XtsmpfJC+Q83lJFcoAEQCLCoygkfaVOOzAa2pWbYof5i8kt5jv2LH/qOBLk1CnAJAJI+oGleYSX2b8WLX2ny9/QAdhqQxbv5WNZeTHKMAEMlDIiKMe5pXYVZSIk2uLslz/13L7aMWsmnvj4EuTUKQAkAkDypfvABv/a4xg++sz+aMI3QeOo/Xvtio5nLiVwoAkTzKzLitYQVmJyXSrnYZXpm1gS6vqbmc+I8CQCSPiy2cj+G9GjHqnuvZd8TXXO4fn6q5nFw5BYBIkOhQuywpSYn0aFSBkamb6Tx0Ll9tVXM5+fUUACJBpFiBaP7Zox7vPNCUU2fPcseohfzlo9X8ePxUoEuTIKQAEAlCLavHMnNgAve3upr/LN5Oh8FpzFm/N9BlSZBRAIgEqYIxUfzlllpMeaQFhfJFce9bS0h6bzkHflJzOckeBYBIkGtUqQTT+7ei/001mLbiO9ompzJ95XdqJyGXpAAQCQH5oiJJancN/328FeVLFKDfu8t46O2l7Dl8PNClSR6mABAJIdeVK8qHj7Tgmc41Sd2QQdvkVN5b8q3eDch5KQBEQkxUZAR9E6oxc2ACtcoV5ckpq7j7jcV8u0/N5eR/KQBEQlSV2EJMfLAZf7utLit3HqLDkDTenLeVM2ouJ55LBoCZjTWzvWa2+gLbzcyGmdkmM1tpZo288QZmttDM1njjd2baZ5yZbTWz5d6jgd9mJCI/i4gwejWtREpSAs2rleLF6Wv5zYgFbNij5nKSvXcA4/At9XghnYAa3qMvMMIbPwr0ds7V9vYfYmbFM+33B+dcA++x/DLrFpHLUK5YAd7sE8/Qng34dv9Rbh42l2Gfb+TkaTWXC2eXDADnXBpwsb837wpMcD6LgOJmVs45t8E5t9E7xnfAXiDOH0WLyOUzM7o2KE/KoAQ61SlHcsoGurw2jxU7Dga6NAkQf1wDKA/syPR8pzf2MzNrAsQAmzMNv+ydGhpsZvkudHAz62tm6WaWnpGR4YdyRcJbqcL5GHZXQ97oHc/Bo6e47fX5/G3GOo6dVHO5cJPjF4HNrBzwNnCvc+7c+82ngZpAY6Ak8OSF9nfOjXbOxTvn4uPi9AZCxF/a1irDrKQEejapxOi0LXQamsbCzfsCXZbkIn8EwC6gYqbnFbwxzKwo8AnwJ+/0EADOud3eKaMTwFtAEz/UISKXqWj+aP52W13efbApDrhrzCKembqKw2ouFxb8EQDTgN7e3UDNgEPOud1mFgNMxXd9YHLmHbx3BZiZAd2A895hJCK5o0W1WD4bkMCDra9m0lff0j45jS++2RPosiSHZec20InAQuBaM9tpZveb2cNm9rD3khnAFmATMAZ41Bu/A0gAfnee2z3fMbNVwCogFnjJbzMSkV+lQEwkf7q5Fh8+2pJiBaK5b1w6AyYtY9+RE4EuTXKIBdOfiMfHx7v09PRAlyES8k6ePsuILzfz2pyNFMkfzbO31qJL/avwvWmXYGNmS51z8VnH9ZfAIvILMVERDGhbg0/6t6ZSyYIMmLScB8ans/vQsUCXJn6kABCRC7qmTBGmPNKCP998HfM3/0D75DTeXfwtZ9VOIiQoAETkoiIjjAdaV2XWwETqVijGM1NX0euNRWz74adAlyZXSAEgItlSqVRB3nmgKf/oXpc1uw7TcWgaY9K2cPqM2kkEKwWAiGSbmdGzSSVSkhJpVT2Ol2es4zcjFvDN94cDXZr8CgoAEblsZYvlZ0zv63mtV0N2HjjGLcPmkZyygROn1U4imCgARORXMTNuqXcVs5MSubX+VQz7fCO3vjqPZd8eCHRpkk0KABG5IiUKxTD4zga89bvG/Hj8NN1HLODF6Ws5evJ0oEuTS1AAiIhf3FCzNLMGJXB300q8OW8rHYakMX/TD4EuSy5CASAiflMkfzQvdavLe32bERURwd1vLOapKSs5dEzN5fIiBYCI+F3TqqX4dEBrHk6sxgdLd9IuOZVZa74PdFmShQJARHJE/uhInupUk48ebUmpwvno+/ZS+r37NT+ouVyeoQAQkRxVt0IxpvVryRPtr2HWmj20TU5l6rKdBFMjylClABCRHBcdGUG/G2swY0ArqsYWYtB7K7h33BJ2HVRzuUBSAIhIrqleuggfPNyCZ2+txeIt+2mfnMrbi7aruVyAKABEJFdFRhj3tryaWYMSaFS5BH/5aDU9Ry9iS8aRQJcWdrIVAGY21sz2mtl5l270loMcZmabzGylmTXKtK2PmW30Hn0yjV9vZqu8fYaZVpoQCSsVSxZkwn1N+FePenzz/WE6DZ3LyNTNai6Xi7L7DmAc0PEi2zsBNbxHX2AEgJmVBJ4FmuJb+P1ZMyvh7TMCeDDTfhc7voiEIDPj9viKzE5KpM21cfzj02/o9vp81n6n5nK5IVsB4JxLA/Zf5CVd8S3+7pxzi4Di3sLvHYAU59x+59wBIAXo6G0r6pxb5Hy3AkzAtzi8iISh0kXzM+qeeEbc3YjvD52gy2vzeGXmeo6fUnO5nOSvawDlgR2Znu/0xi42vvM8479gZn3NLN3M0jMyMvxUrojkRZ3qlmN2UgJdG5TntTmbuHnYXJZuv9jvnnIl8vxFYOfcaOdcvHMuPi4uLtDliEgOK14whn/fUZ/x9zXh+Kmz9Bi5kOemreGnE2ou52/+CoBdQMVMzyt4Yxcbr3CecRERABKviWPmoAR6N6vM+IXbaD84jbQNOgvgT/4KgGlAb+9uoGbAIefcbmAm0N7MSngXf9sDM71th82smXf3T2/gYz/VIiIhonC+KJ7vWof3H2pOvugIeo/9iic+WMGho2ou5w9R2XmRmU0E2gCxZrYT35090QDOuZHADKAzsAk4CtzrbdtvZi8CS7xDveCcO3dC71F8dxcVAD71HiIiv9C4Sklm9G/NsM83MiptC6kbMnixa2061ikX6NKCmgVTP474+HiXnp4e6DJEJIBW7zrEHyevZO3uw3SqU5bnu9amdJH8gS4rTzOzpc65+Kzjef4isIhIZnXKF+Pjfi35Q4dr+fybvbRLTmPyUjWX+zUUACISdKIjI3jshurM6N+aGqUL88QHK+jz1hJ2Hjga6NKCigJARIJW9dKFef+h5rzQtTZLt+2n/eA0xi/YpuZy2aQAEJGgFhFh9G5ehZmDEoivUpJnp63hjlEL2bRXzeUuRQEgIiGhQomCjL+3Mf++vT4b9x6h89C5DJ+ziVNqLndBCgARCRlmxm+ur8DspETa1irNv2aup+tr81m961CgS8uTFAAiEnLiiuTj9buvZ+RvG5Fx5ARdh8/nn599o+ZyWSgARCRkdaxTjtmDEunesDwjvtxM56FzWbJNzeXOUQCISEgrVjCaf91en7fvb8LJM2e5feRC/vrxao6ouZwCQETCQ+saccwcmMC9Lavw9qLtdBicxpfr9wa6rIBSAIhI2CiUL4pnb63N5IdbUCAmkt+9tYSk95dz4KeTgS4tIBQAIhJ2rq9cgk/6t+LxG6szbfl3tBucyoxVu8OunYQCQETCUr6oSH7f/lqm9WtFuWIFePSdr3n4P0vZe/h4oEvLNQoAEQlrta4qytRHW/BUp5p8uT6DtsmpvL9kR1i8G1AAiEjYi4qM4OHEanw6oDU1yxblj1NWcs+bX7Fjf2g3l1MAiIh4qsYVZlLfZrzYrQ7Ldxyk/eA0xs7bypkQbS6XrQAws45mtt7MNpnZU+fZXtnMPjezlWb2pZlV8MZvMLPlmR7Hzaybt22cmW3NtK2BPycmIvJrREQY9zSrzKxBCTStWpIXpq/l9pEL2Ljnx0CX5neXXBHMzCKBDUA7YCe+5R3vcs6tzfSaD4DpzrnxZnYjcK9z7p4sxymJb8nICs65o2Y2zttncnaL1YpgIpKbnHN8vPw7nv/vGn46cYbHb6zOw22qER0ZXCdPrmRFsCbAJufcFufcSWAS0DXLa2oBX3ifzznPdoAewKfOudA+qSYiIcPM6NawPClJibSvXYZ/p2zg1lfnsWpnaDSXy04AlAd2ZHq+0xvLbAXQ3fv8NqCImZXK8pqewMQsYy97p40Gm1m+831xM+trZulmlp6RkZGNckVE/Cu2cD5e69WI0fdcz/6fTtJ1+Dz+/um6oG8u56/3MU8AiWa2DEgEdgE//5cxs3JAXWBmpn2eBmoCjYGSwJPnO7BzbrRzLt45Fx8XF+enckVELl/72mVJSUrkjviKjErdQschaSzasi/QZf1q2QmAXUDFTM8reGM/c85955zr7pxrCPzJGzuY6SV3AFOdc6cy7bPb+ZwA3sJ3qklEJE8rViCaf/ymHu880JSzDnqOXsSfpq7ix+OnLr1zHpOdAFgC1DCzq80sBt+pnGmZX2BmsWZ27lhPA2OzHOMuspz+8d4VYGYGdANWX3b1IiIB0rJ6LJ8NbM0Dra5m4lff0n5wGnO+Ca7mcpcMAOfcaaAfvtM364D3nXNrzOwFM+vivawNsN7MNgBlgJfP7W9mVfC9g0jNcuh3zGwVsAqIBV66sqmIiOSugjFR/PmWWkx5pAWF80Vx77glDJy0jP1B0lzukreB5iW6DVRE8qoTp8/w+pzNDJ+ziWIFonmuS21uqVcO30mOwLqS20BFROQS8kVFMqjdNUzv34ryJQrw+MRlPDhhKd8fyrvN5RQAIiJ+VLNsUT58pAV/6nwd8zZl0C45lYlffZsnm8spAERE/CwqMoIHE6ry2YAEapcvytMfrqLXmMVs3/dToEv7HwoAEZEcUiW2EO8+0Iy/3VaX1bsO0WFIGm/M3ZJnmsspAEREclBEhNGraSVmJSXQslosL32yju4jFrD++8A3l1MAiIjkgnLFCvBGn3iG9mzAjv1HueXVuQyZvYGTp88GrCYFgIhILjEzujYoT8qgBDrXLceQ2Ru59dV5LN9xMCD1KABERHJZqcL5GNqzIW/2iefQsVN0f30+L3+ylmMnc7e5nAJARCRAbrquDLOSEujZpBJj5m6lw5A0Fmz+Ide+vgJARCSAiuaP5m+31WXig80wg15jFvP0h6s4nAvN5RQAIiJ5QPNqpfhsQAJ9E6ry3pJvaZecyuy1e3L0ayoARETyiAIxkTzT+TqmPtqSEgVjeGBCOv0nLmPfkRM58vUUACIieUz9isWZ1q8VSe2u4dPVu2mbnMrCzf5feEYBICKSB8VERdD/php80r81dcoXo0psQb9/jSi/H1FERPzmmjJFePv+pjly7Gy9AzCzjma23sw2mdlT59le2cw+9xZ4/9LMKmTadsbMlnuPaZnGrzazxd4x3/NWGxMRkVxyyQAws0hgONAJqAXcZWa1srzsFWCCc64e8ALw90zbjjnnGniPLpnG/wkMds5VBw4A91/BPERE5DJl5x1AE2CTc26Lc+4kMAnomuU1tYAvvM/nnGf7//DWAb4RmOwNjce3LrCIiOSS7ARAeWBHpuc7vbHMVgDdvc9vA4qYWSnveX4zSzezRWbWzRsrBRz01hu+0DFFRCQH+esuoCeARDNbBiQCu4BzTS0qe2tR9gKGmFm1yzmwmfX1AiQ9IyPDT+WKiEh2AmAXUDHT8wre2M+cc98557o75xoCf/LGDnofd3kftwBfAg2BfUBxM4u60DEzHXu0cy7eORcfFxeXzWmJiMilZCcAlgA1vLt2YoCewLTMLzCzWDM7d6yngbHeeAkzy3fuNUBLYK3zLY45B+jh7dMH+PhKJyMiItl3yQDwztP3A2YC64D3nXNrzOwFMzt3V08bYL2ZbQDKAC9749cB6Wa2At8P/H8459Z6254EksxsE75rAm/6aU4iIpINlhdXqr8QM8sAtgOxQO71TM17wnn+4Tx3CO/5h/Pc4crmX9k594tz6EEVAOeYWbp3YTkshfP8w3nuEN7zD+e5Q87MX72ARETClAJARCRMBWsAjA50AQEWzvMP57lDeM8/nOcOOTD/oLwGICIiVy5Y3wGIiMgVUgCIiISpoAuAS61NEGrMbKyZ7TWz1ZnGSppZiplt9D6WCGSNOcXMKprZHDNba2ZrzGyANx7y8zez/Gb2lZmt8Ob+vDceNutomFmkmS0zs+ne83Ca+zYzW+Wto5Lujfn9+z6oAiCbaxOEmnFAxyxjTwGfO+dqAJ97z0PRaeD3zrlaQDPgMe//dzjM/wRwo3OuPtAA6GhmzQivdTQG4Os+cE44zR3gBm8dlXP3/vv9+z6oAoDsrU0QUpxzacD+LMNd8a2hACG8loJzbrdz7mvv8x/x/TAoTxjM3/kc8Z5Gew9HmKyj4a0qeDPwhvdca4jkwPd9sAVAdtYmCAdlnHO7vc+/x9d/KaSZWRV8nWQXEybz906BLAf2AinAZsJnHY0hwB+Bs97zcFtDxAGzzGypmfX1xvz+fa9F4YOcc86ZWUjfy2tmhYEpwEDn3GHfL4M+oTx/59wZoIGZFQemAjUDW1HuMLNbgL3OuaVm1ibA5QRKK+fcLjMrDaSY2TeZN/rr+z7Y3gFccm2CMLHHzMoBeB/3BrieHGNm0fh++L/jnPvQGw6b+cPPa2vMAZqTzXU0glxLoIuZbcN3mvdGYCjhMXfgf9ZR2Ysv/JuQA9/3wRYAl1ybIExMw7eGAoTwWgreed83gXXOueRMm0J+/mYW5/3mj5kVANrhuwYS8utoOOeeds5VcM5Vwfdv/Avn3N2EwdwBzKyQmRU59znQHlhNDnzfB91fAptZZ3znByOBsc65ly++R3Azs4n41luIBfYAzwIfAe8DlfC1x77DOZf1QnHQM7NWwFxgFf//XPAz+K4DhPT8zawevgt9kfh+UXvfOfeCmVXF91txSWAZ8Fvn3InAVZqzvFNATzjnbgmXuXvznOo9jQLedc69bL511v36fR90ASAiIv4RbKeARETETxQAIiJhSgEgIhKmFAAiImFKASAiEqYUACIiYUoBICISpv4fulVGial2LigAAAAASUVORK5CYII=",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "atp = np.linspace(2, 50, 10)\n",
    "biomass = []\n",
    "for i in atp:\n",
    "    m_fc.reactions.get_by_id(\"ATPase\").bounds = (i, 1000)\n",
    "    biomass.append(pfba(m_fc).to_frame()[\"fluxes\"][\"BIOMASS\"])\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "ax.plot(atp, biomass)\n",
    "plt.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## SBML export\n",
    "\n",
    "To export a model SBML including all desired annotations we use the `to_sbml` function to create a sbml model which can be re-used and tested using MEMOTE."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [],
   "source": [
    "fastacyc.to_sbml(Path(\"..\") / \"generated\" / \"models\" / \"ecoli.xml\")\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "interpreter": {
   "hash": "5d2862dc9757a2bae5a59ffe521fdff6fdfe254fa3ab6eceec15fc2a32be6608"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
