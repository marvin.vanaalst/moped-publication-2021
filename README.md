# Moped 2021 publication

## File list to download

For legal reasons we cannot supply the metacyc database flatfiles. 
They can be obtained from [metacyc](https://metacyc.org/). 

We have used version 25.1. 
The flatfiles should be placed into `data/metacyc/25.1/*`, such that the scripts can find e.g. `data/metacyc/25.1/compounds.dat`


## Install & run

If you have docker installed, you can use the Dockerfile to get a no-hassle setup, otherwise follow the `Manual` instructions below

### Docker

`docker build --tag moped .`  
`docker run -p 8888:8888 moped`  
Open the link in the terminal and explore the notebooks in your browser (read-only)



### Manual

You can install moped, jupyter and memote using `pip install .`

However, you will still need to install NCBI Blast if you want to use the genome / proteome reconstruction methods

If you are using conda or mamba, you can install blast from bioconda: 

`conda install -c bioconda blast==2.12.0`  
or  
`mamba install -c bioconda blast==2.12.0`  

If you want to install BLAST yourself:
- download the latest blast version from [NCBI](ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/)
- Extract the downloaded folder to a path of your liking
- Add blast to your path e.g. by adding the following line to your .bashrc
  - `export PATH="your_blast_directory/bin:$PATH"`

Start a notebook server `jupyter notebook moped2021`


### Memote

In `generated/`

`memote report snapshot --filename memote/ecoli_moped.html models/ecoli.xml`