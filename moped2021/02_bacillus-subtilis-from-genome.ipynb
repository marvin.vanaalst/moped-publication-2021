{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Bacillus subtilis reconstruction\n",
    "\n",
    "To construct a metabolic model for Bacillus subtilis, we first import the MetaCyc database into moped from its PGDB files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1.9.0\n"
     ]
    }
   ],
   "source": [
    "import pkg_resources\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "from pathlib import Path\n",
    "from moped import Reaction, Model\n",
    "from moped.utils import blast_is_installed\n",
    "from cobra.flux_analysis import pfba\n",
    "print(pkg_resources.get_distribution(\"moped\").version)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Model: Model\n",
       "    compounds: 12840\n",
       "    reactions: 18393"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "metacyc = Model()\n",
    "metacyc.read_from_pgdb(pgdb_path=Path(\"..\") / \"data\" / \"metacyc\" / \"25.1\" / \"data\")\n",
    "metacyc\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create model from proteome/genome or database\n",
    "\n",
    "If we have BLAST installed, we can use the functions `create_submodel_from_proteome` or `create_submodel_from_genome` to import a proteome/genome fasta file and BLAST it against the MetaCyc database to construct a model of all reactions that can be found in the genome sequence.\n",
    "\n",
    "Alternatively, its possible to import the BioCyc PGDB files of B. subtilis using `read_from_pgdb` and `update_from_reference` to avoid version discrepancies."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Model: bacillus\n",
       "    compounds: 1388\n",
       "    reactions: 1183"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "if not blast_is_installed():\n",
    "    raise OSError(\"Could not find blast. Please make sure its on your path.\")\n",
    "\n",
    "fastacyc = metacyc.create_submodel_from_proteome(\n",
    "    \"../data/proteomes/bacillus.fasta\",\n",
    "    name=\"bacillus\",\n",
    "    cache_blast_results=True,\n",
    "    max_evalue=1e-03,\n",
    "    min_coverage=40,\n",
    "    min_pident=40,\n",
    ")\n",
    "fastacyc\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Topological gapfilling\n",
    "\n",
    "In order to apply topological gapfilling to enable our model to produce biomass compounds, we apply cofactor and reversibility duplication for both draft and reference objects (see manuscript).\n",
    "\n",
    "After getting a the template biomass compounds using `get_biomass_template`, we define a minimal medium seed including weak mock cofactors.\n",
    "\n",
    "The function `get_gapfilling_reactions` uses Meneco gapfilling to find a minimal set of missing reactions from the reference moped object to produce all targets, and returns the list of reaction IDs in a list.\n",
    "A result of previous gapfilling is provided, but can be commented out for new calculations.\n",
    "\n",
    "These reactions are then added to the draft moped object from the reference moped object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "medium = [\n",
    "    \"ALPHA-GLUCOSE_c\",\n",
    "    \"WATER_c\",\n",
    "    \"PROTON_c\",\n",
    "    \"OXYGEN-MOLECULE_c\",\n",
    "    \"Pi_c\",\n",
    "    \"SULFATE_c\",\n",
    "    \"AMMONIA_c\",\n",
    "] \n",
    "\n",
    "seed = medium + metacyc.get_weak_cofactor_duplications()\n",
    "\n",
    "biomass_comp = fastacyc.get_biomass_template()\n",
    "\n",
    "# Manual additions to be always sure they are there\n",
    "for rxn in list(metacyc.pathways[\"GLYCOLYSIS\"]):\n",
    "    if rxn not in list(fastacyc.reactions):\n",
    "        fastacyc.add_reaction_from_reference(metacyc, rxn)\n",
    "\n",
    "for rxn in list(metacyc.pathways[\"TCA\"]):\n",
    "    if rxn not in list(fastacyc.reactions):\n",
    "        fastacyc.add_reaction_from_reference(metacyc, rxn)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "metacyc.cofactor_duplication()\n",
    "metacyc.reversibility_duplication()\n",
    "fastacyc.cofactor_duplication()\n",
    "fastacyc.reversibility_duplication()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Topological gapfilling can take quite a while, so we cached the results here\n",
    "gapfill_result = [\n",
    "    \"PLASTOQUINOL--PLASTOCYANIN-REDUCTASE-RXN__var__0_c_p__cof__\",\n",
    "    \"RXN0-5224_c__rev__\",\n",
    "    \"RXN0-742_c__cof__\",\n",
    "    \"ATPSYN-RXN_c_p__cof____rev__\",\n",
    "    \"RXN0-1401_c__cof__\",\n",
    "    \"RXN-12303__var__0_c__cof__\",\n",
    "    \"RXN0-5114_c\",\n",
    "    \"RXN-20084_c__cof__\",\n",
    "    \"RXN0-1402_c__cof__\",\n",
    "    \"PYRUVDEH-RXN_c__cof__\",\n",
    "]\n",
    "if gapfill_result is None:\n",
    "    gapfill_result = fastacyc.get_gapfilling_reactions(\n",
    "        reference_model=metacyc,\n",
    "        seed=seed,\n",
    "        targets=[\"UTP_c\", \"ATP_c\", \"GTP_c\", \"CTP_c\"],\n",
    "        verbose=True,\n",
    "    )\n",
    "for reaction_id in gapfill_result:\n",
    "    fastacyc.add_reaction_from_reference(reference_model=metacyc, reaction_id=reaction_id)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Topological gapfilling can take quite a while, so we cached the results here\n",
    "gapfill_result = [\n",
    "    \"PRIBFAICARPISOM-RXN_c\",\n",
    "    \"TYROSINE-AMINOTRANSFERASE-RXN_c__rev__\",\n",
    "    \"NADH-KINASE-RXN_c\",\n",
    "    \"RXN-18377_c\",\n",
    "    \"RXN-8001_c__cof__\",\n",
    "    \"HOMOSERKIN-RXN_c__cof__\",\n",
    "    \"6.3.1.4-RXN_c\",\n",
    "    \"NICONUCADENYLYLTRAN-RXN_c\",\n",
    "    \"ADENYLYLSULFATE-REDUCTASE-RXN_c__cof____rev__\",\n",
    "    \"DIHYDROFOLATESYNTH-RXN_c__cof__\",\n",
    "    \"PRAISOM-RXN_c\",\n",
    "    \"RXN0-7066_c\",\n",
    "    \"PRTRANS-RXN_c__rev__\",\n",
    "    \"ATPPHOSPHORIBOSYLTRANS-RXN_c__rev__\",\n",
    "    \"ASPDECARBOX-RXN_c\",\n",
    "    \"HOMOCYSMETB12-RXN__var__1_c\",\n",
    "    \"GLUTAMIDOTRANS-RXN_c\",\n",
    "    \"PREPHENATE-DEHYDROGENASE-NADP+-RXN_c\",\n",
    "    \"ORNITHINE-CYCLODEAMINASE-RXN_c\",\n",
    "    \"QUINOLINATE-SYNTHE-MULTI-RXN_c\",\n",
    "    \"TRYPSYN-RXN_c\",\n",
    "    \"CHORISMATE-SYNTHASE-RXN_c\",\n",
    "    \"RXN-11334__var__1_c__cof__\",\n",
    "    \"2-DEHYDROPANTOATE-REDUCT-RXN_c\",\n",
    "    \"HISTAMINOTRANS-RXN_c__rev__\",\n",
    "    \"1.5.1.20-RXN__var__2_c__cof____rev__\",\n",
    "    \"HOMOSERDEHYDROG-RXN__var__0_c\",\n",
    "    \"RXN-7737_c__rev__\",\n",
    "    \"GLUTKIN-RXN_c\",\n",
    "]\n",
    "if gapfill_result is None:\n",
    "    gapfill_result = fastacyc.get_gapfilling_reactions(\n",
    "        reference_model=metacyc,\n",
    "        seed=seed,\n",
    "        targets=biomass_comp,\n",
    "        verbose=True,\n",
    "    )\n",
    "for reaction_id in gapfill_result:\n",
    "    fastacyc.add_reaction_from_reference(reference_model=metacyc, reaction_id=reaction_id)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Remove duplications\n",
    "fastacyc.remove_cofactor_duplication()\n",
    "fastacyc.remove_reversibility_duplication()\n",
    "metacyc.remove_cofactor_duplication()\n",
    "metacyc.remove_reversibility_duplication()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Flux Balance Analysis (FBA)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To test our model using FBA later, we define a biomass reaction, which is a simplified version of the E. coli iJO1366 reaction, and an ATPase reaction as an atp maintenance requirement, and add both to the draft moped object.\n",
    "\n",
    "Sometimes not all Biomass compounds are included in draft moped objects. To avoid problems, we also add any missing compounds from to the draft object from the reference object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "atpase_rxn = Reaction(\n",
    "    id=\"ATPase\",\n",
    "    stoichiometries={\"ATP_c\": -1, \"WATER_c\": -1, \"ADP_c\": 1, \"Pi_c\": 1, \"PROTON_c\": 1},\n",
    "    bounds=(0.0, 1000.0),\n",
    ")\n",
    "biomass_rxn = Reaction(\n",
    "    id=\"BIOMASS\",\n",
    "    stoichiometries=biomass_comp,\n",
    "    bounds=(0.0, 1000.0),\n",
    ")\n",
    "\n",
    "excluded = set(medium + [\"CARBON-DIOXIDE_c\"])\n",
    "cytosol = set(fastacyc.get_compounds_of_compartment(\"CYTOSOL\")).difference(excluded)\n",
    "\n",
    "for compound_id in biomass_comp:\n",
    "    if compound_id not in fastacyc.compounds:\n",
    "        fastacyc.add_compound_from_reference(reference_model=metacyc, compound_id=compound_id)\n",
    "\n",
    "for compound_id in excluded:\n",
    "    fastacyc.add_transport_reaction(compound_id=compound_id, compartment_id=\"EXTRACELLULAR\")\n",
    "    fastacyc.add_medium_component(compound_id=compound_id, extracellular_compartment_id=\"EXTRACELLULAR\")\n",
    "\n",
    "\n",
    "fastacyc.add_reaction(biomass_rxn)\n",
    "fastacyc.add_reaction(atpase_rxn)\n",
    "\n",
    "fastacyc.reactions[\"EX_ALPHA-GLUCOSE_e\"].bounds = (-10, 10)\n",
    "fastacyc.objective = {\"BIOMASS\": 1}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to apply Flux Balance Analysis to our model, we add exchange reactions that can introduce compounds into our model for all medium seed compounds using `add_transport_reaction` and `add_medium_component`.  \n",
    "In this model, we allow Carbon Dioxide to leave the model via a respective efflux reaction.\n",
    "We define a new variable in which we export our moped object into a CobraPy object, and set the objective coefficient of our Biomass reaction to 1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Scaling...\n",
      " A: min|aij| =  1.000e+00  max|aij| =  1.000e+00  ratio =  1.000e+00\n",
      "Problem data seem to be well scaled\n"
     ]
    }
   ],
   "source": [
    "all_exporters = fastacyc.copy()\n",
    "\n",
    "for compound_id in cytosol:\n",
    "    all_exporters.add_transport_reaction(compound_id, \"EXTRACELLULAR\")\n",
    "    all_exporters.add_efflux(compound_id, \"EXTRACELLULAR\")\n",
    "\n",
    "m_ex = all_exporters.to_cobra()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>fluxes</th>\n",
       "      <th>reduced_costs</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>EX_Pi_e</th>\n",
       "      <td>-3.758297</td>\n",
       "      <td>2.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>EX_ALPHA-GLUCOSE_e</th>\n",
       "      <td>-10.000000</td>\n",
       "      <td>48.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>EX_OXYGEN-MOLECULE_e</th>\n",
       "      <td>-13.029337</td>\n",
       "      <td>2.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>EX_AMMONIA_e</th>\n",
       "      <td>-7.074382</td>\n",
       "      <td>2.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>EX_PROTON_e</th>\n",
       "      <td>7.982044</td>\n",
       "      <td>-2.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>EX_SULFATE_e</th>\n",
       "      <td>-0.274613</td>\n",
       "      <td>2.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>EX_WATER_e</th>\n",
       "      <td>31.472503</td>\n",
       "      <td>-2.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>EX_CARBON-DIOXIDE_e</th>\n",
       "      <td>12.837641</td>\n",
       "      <td>-2.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>EX_FORMALDEHYDE_e</th>\n",
       "      <td>0.146767</td>\n",
       "      <td>-2.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>EX_ACET_e</th>\n",
       "      <td>12.335861</td>\n",
       "      <td>-2.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>EX_CPD-15403_e</th>\n",
       "      <td>0.118083</td>\n",
       "      <td>-2.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>EX_PPI_e</th>\n",
       "      <td>1.237324</td>\n",
       "      <td>-2.0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "                         fluxes  reduced_costs\n",
       "EX_Pi_e               -3.758297            2.0\n",
       "EX_ALPHA-GLUCOSE_e   -10.000000           48.0\n",
       "EX_OXYGEN-MOLECULE_e -13.029337            2.0\n",
       "EX_AMMONIA_e          -7.074382            2.0\n",
       "EX_PROTON_e            7.982044           -2.0\n",
       "EX_SULFATE_e          -0.274613            2.0\n",
       "EX_WATER_e            31.472503           -2.0\n",
       "EX_CARBON-DIOXIDE_e   12.837641           -2.0\n",
       "EX_FORMALDEHYDE_e      0.146767           -2.0\n",
       "EX_ACET_e             12.335861           -2.0\n",
       "EX_CPD-15403_e         0.118083           -2.0\n",
       "EX_PPI_e               1.237324           -2.0"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df = pfba(m_ex).to_frame()\n",
    "ex = df[df.index.str.startswith(\"EX\")]\n",
    "exporters = ex[ex[\"fluxes\"].abs() > 1e-3]\n",
    "exporters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'ACET_c', 'CPD-15403_c', 'FORMALDEHYDE_c', 'PPI_c'}"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "new_to_export = {cpd for i in exporters.index if (cpd := f\"{i[3:-2]}_c\") not in excluded}\n",
    "\n",
    "for compound_id in new_to_export:\n",
    "    fastacyc.add_transport_reaction(compound_id=compound_id, compartment_id=\"EXTRACELLULAR\")\n",
    "    fastacyc.add_medium_component(compound_id=compound_id, extracellular_compartment_id=\"EXTRACELLULAR\")\n",
    "new_to_export"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.655940530332511"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "m_fc = fastacyc.to_cobra()\n",
    "m_fc.optimize()[\"BIOMASS\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## ATP maintenance scan\n",
    "\n",
    "Good indicator for thermodynamically infeasible loops"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAXQAAAD4CAYAAAD8Zh1EAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjUuMSwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy/YYfK9AAAACXBIWXMAAAsTAAALEwEAmpwYAAAi/0lEQVR4nO3dd3gVddrG8e+TRuhFAtIkSBEDUkOHxEJHAREVbNhAFKTEXcu7u65td13dDU2QYq+AoIBKC5aEDqF3CAjShFAE6e33/pG4G9kgAU4yyTn357q4yMwZM8/v2uPt7JzMHXPOISIieV+Q1wOIiIhvKNBFRPyEAl1ExE8o0EVE/IQCXUTET4R4deKSJUu6yMhIr04vIpInLVmyZJ9zLiKz1zwL9MjISJKTk706vYhInmRm2y70mm65iIj4CQW6iIifUKCLiPgJBbqIiJ9QoIuI+AkFuoiIn1Cgi4j4iTwX6JtTj/DvmRs4cfqs16OIiOQqeS7QE9buYdi3KXQYOpsl2w54PY6ISK6R5wK9d2xl3n+4ISdOn6PryPm8MGUNR0+e8XosERHP5blAB4itFsGMgTE80Lgi78/fSutBSSRtTPV6LBERT+XJQAcolC+EFzvVZPxjTcgXGsQD7yziD5+t4Odjp7weTUTEE3k20H/VILIEU/u14IkbK/PFsp20jE9i2qrdXo8lIpLj8nygA4SHBvN02+pM7tOM0kXy8fjHS3n8oyXs/eWE16OJiOQYvwj0X9UsV5RJfZrxdNvr+Gb9XlrFJ/FZ8nacc16PJiKS7fwq0AFCg4N44sYqTOvfgmqlC/HHCSt54J1FbD9wzOvRRESyld8F+q8qRxRiXK8mvNSpBku3HaTN4CTem/sD587pal1E/JPfBjpAUJDxQJNIZgyMITqyBC98uZY7R80nZe8vXo8mIuJzWQp0M2trZhvMLMXMnr3AMXeZ2VozW2Nmn/h2zCtTvngB3n+oAf++szabU4/Qfsgchn+Xwumz57weTUTEZy4a6GYWDAwH2gFRQHczizrvmKrAc0Az51wNYIDvR70yZsYd9cuTMDCWVlGleX3GBjq+MZfVOw95PZqIiE9k5Qq9IZDinNvinDsFjAU6nXdMT2C4c+4ggHNur2/H9J2IwvkYfm89Rt5Xn31HTtJp+FxenbZeZV8ikudlJdDLAdszbO9I35dRNaCamc01swVm1jazb2Rmvcws2cySU1O9fVS/bc2rmTUwljvqlWNk4mbaD5nNoh9U9iUieZevPhQNAaoCNwLdgTFmVuz8g5xzo51z0c656IiICB+d+vIVLRDKa11r89EjjTh19hx3jZrPXyat5ojKvkQkD8pKoO8EKmTYLp++L6MdwBTn3Gnn3A/ARtICPk9oXrUkMwbE8HCzSny0cBut4xP5bkOuvWskIpKprAT6YqCqmVUyszCgGzDlvGMmkXZ1jpmVJO0WzBbfjZn9CuYL4fnbopjQuykF8oXw0LuLiRu3nINHVfYlInnDRQPdOXcG6AvMANYB451za8zsJTPrmH7YDGC/ma0FvgP+6Jzbn11DZ6f6FYvzdb/m9Lu5ClNW7KJlfCJfrdyl+gARyfXMq6CKjo52ycnJnpw7q9buOswzE1eyauchWkeV5uXONSldJNzrsUQkgJnZEudcdGav+fWTolcqqmwRvniiKc+1q07ixlRaxicybvGPuloXkVxJgX4RIcFBPBZbmekDYri+TBGembiK+95eyI/7VfYlIrmLAj2LKpUsyNiejXmlc01WbD9Em8FJvD3nB86q7EtEcgkF+iUICjLua1yRmQNjaHxtCV7+ai13vDmPjXtU9iUi3lOgX4ayxfLzzoMNGHx3HbbtP0qHobMZ+s0mTp1R2ZeIeEeBfpnMjM51y5EQF0vbmmWIT9hIxzfmsGL7z16PJiIBSoF+hUoWysew7nUZ80A0B4+d4vYRc/nH1HUcP6WyLxHJWQp0H2kVVZqEuFjublCBUUlbaDckiQVb8uSzVSKSRynQfahIeCj/6FKLTx5txDkH3UYv4P++WMXhE6e9Hk1EAoACPRs0rZJW9vVo80qMXfQjreOT+Hb9Hq/HEhE/p0DPJvnDgvnzrVFMfLwpRfKH8PB7yfQfu4z9R056PZqI+CkFejare01xvnqyBf1vqcrUVbtpNSiJKStU9iUivqdAzwFhIUEMbFWNL59sToXi+en36TJ6fpDMT4dOeD2aiPgRBXoOqn51ET5/ohl/an89c1L20So+kU8XqexLRHxDgZ7DgoOMnjHXMr1/DDXKFeG5z1dxz5iFbNt/1OvRRCSPU6B7JLJkQT7t2Zh/dLmB1TvTyr7emr1FZV8ictkU6B4yM7o3vIaEuFiaVynJK1+vo8ub89jwk8q+ROTSKdBzgauLhjPmgWiGdq/L9gPHuHXYbAYlbFTZl4hcEgV6LmFmdKxdlllxsbS/oQxDvtnErcNms1xlXyKSRQr0XKZEwTCGdKvL2z2iOXz8DF1GzOWVr9aq7EtELkqBnkvdcn1pZsbF0K3hNbw15wfaDE5i3uZ9Xo8lIrmYAj0XKxIeyt9vv4FPezbGDO4Zs5DnPl+psi8RyZQCPQ9oUvkqpveP4bGYaxm3eDut4hOZtVZlXyLyWwr0PCJ/WDDPtb+eSX2aUbxAGI9+kMyTn6rsS0T+S4Gex9QqX4wpfZsT16oa01fvpmV8IpOW7VR9gIgo0POisJAg+t1Sla/7taDiVQUZMG45j7yfzK6fj3s9moh4SIGeh1UrXZiJjzflL7dGMX/zfloPSuKjBds4p/oAkYCkQM/jgoOMR5pXYsaAGGpXKMqfJ62m+5gF/LBPZV8igUaB7ieuuaoAHz3SiNfuqMXa3YdpOziJUYmbOXNW9QEigSJLgW5mbc1sg5mlmNmzmbz+oJmlmtny9D+P+n5UuRgz464GFZgVF0tMtQj+MW09t4+Yx9pdh70eTURywEUD3cyCgeFAOyAK6G5mUZkcOs45Vyf9z1s+nlMuQeki4Yy+vz7D76nH7kPH6fjGHP49cwMnz6g+QMSfZeUKvSGQ4pzb4pw7BYwFOmXvWHKlzIwOtcqQMDCWjrXLMuzbFDoMncOSbQe9Hk1EsklWAr0csD3D9o70fee7w8xWmtkEM6uQ2Tcys15mlmxmyampqZcxrlyq4gXDiL+7Du8+1IBjJ8/QdeQ8XvxyDcdOnfF6NBHxMV99KPolEOmcqwUkAO9ndpBzbrRzLto5Fx0REeGjU0tW3HRdKWbGxXJ/44q8O3crrQclMWeTyr5E/ElWAn0nkPGKu3z6vv9wzu13zv36DPpbQH3fjCe+VChfCC91qsn4x5oQGhzEfW8v5OkJKzh0TGVfIv4gK4G+GKhqZpXMLAzoBkzJeICZlcmw2RFY57sRxdcaVirBtP4tePzGykxcupOWgxKZvvonr8cSkSt00UB3zp0B+gIzSAvq8c65NWb2kpl1TD+sn5mtMbMVQD/gwewaWHwjPDSYZ9pWZ3KfZkQUykfvj5bQ5+OlpP6isi+RvMq8KnWKjo52ycnJnpxbfuv02XOMTtrCkFmbyB8WzPO3RtGlXjnMzOvRROQ8ZrbEORed2Wt6UlQIDQ6iz01VmNq/BVVKFeKpz1bw4LuL2amyL5E8RYEu/1GlVCE+e6wJL9wWxeKtB2gdn8gH87eq7Eskj1Cgy28EBRkPNksr+6pXsTjPT17D3aPnszn1iNejichFKNAlUxVKFOCDhxvyrztrs3HPEdoNmc2I71M4rbIvkVxLgS4XZGZ0rV+ehLgYbqleitemb6Dz8Lms3nnI69FEJBMKdLmoUoXDefO++rx5bz32HD5Jp+FzeX3Gek6cVtmXSG6iQJcsa3dDGWbFxdClbjmGf7eZ9kNnk7z1gNdjiUg6BbpckmIFwnj9ztp88HBDTp4+x52j5vPXyas5clJlXyJeU6DLZYmpFsHMgTH0aBLJBwu20WZQEokb1aAp4iUFuly2gvlCeKFjDSb0bkJ4aBA93lnEU+NX8POxU16PJhKQFOhyxepXLMHX/VrQ96YqTF6+k5bxSUxbtdvrsUQCjgJdfCI8NJg/tLmOyX2bcXXRfDz+8VJ6f7iEvYdPeD2aSMBQoItP1ShblElPNOOZttX5dsNeWsYn8lnydrwqgRMJJAp08bmQ4CAev7Ey0/u3oPrVRfjjhJU88M4ith845vVoIn5NgS7Z5tqIQozt1ZiXO9Vg6baDtBmcxLtzf+Csyr5EsoUCXbJVUJBxf5NIZsbF0iCyBC9+uZa7Rs0nZe8vXo8m4ncU6JIjyhXLz3sPNSD+rtpsTj1C+yFzeOPbTSr7EvEhBbrkGDOjS73yJAyMpVWN0vxr5kY6vqGyLxFfUaBLjosonI/h99Rj1P312X8krezr1Wkq+xK5Ugp08UybGleTEBdL13rlGZm4mfZDZrPoB5V9iVwuBbp4qmj+UP7ZtRYfP9qI0+fOcdeo+fxl0mp+OXHa69FE8hwFuuQKzaqUZMaAGB5uVomPFqaVfX23Ya/XY4nkKQp0yTUKhIXw/G1RTHy8KQXzhfDQu4uJG7ecg0dV9iWSFQp0yXXqXVOcr/o1p9/NVZiyYhct4xP5auUu1QeIXIQCXXKlfCHBxLW+ji+fbE7ZYvnp+8kyHvtwCXtU9iVyQQp0ydWuL1OEL55oynPtqpO4MZWW8YmMW/yjrtZFMqFAl1wvJDiIx2IrM31ADFFlivDMxFXc9/ZCftyvsi+RjBTokmdUKlmQT3s25m+312TF9kO0GZzE23NU9iXyqywFupm1NbMNZpZiZs/+znF3mJkzs2jfjSjyX0FBxr2NKpIQF0OTylfx8ldr6TpyHpv2qOxL5KKBbmbBwHCgHRAFdDezqEyOKwz0Bxb6ekiR85Upmp+3e0QzpFsdtu47Svuhsxn6zSZOnVHZlwSurFyhNwRSnHNbnHOngLFAp0yOexn4J6AfQ5AcYWZ0qlOOWXGxtK1ZhviEjXR8Yw4rtv/s9WginshKoJcDtmfY3pG+7z/MrB5QwTn39e99IzPrZWbJZpacmpp6ycOKZOaqQvkY1r0uYx6I5uCxU9w+Yi7/mLqO46dU9iWB5Yo/FDWzICAeeOpixzrnRjvnop1z0REREVd6apHfaBVVmoS4WO5uUIFRSVtoNySJBVv2ez2WSI7JSqDvBCpk2C6fvu9XhYGawPdmthVoDEzRB6PihSLhofyjSy0+ebQR5xx0G72AP32xSmVfEhCyEuiLgapmVsnMwoBuwJRfX3TOHXLOlXTORTrnIoEFQEfnXHK2TCySBU3Ty756tqjEp4t+pPWgJL5dv8frsUSy1UUD3Tl3BugLzADWAeOdc2vM7CUz65jdA4pcrvxhwfypQxSfP9GMIuGhPPxeMv3HLmP/kZNejyaSLcyrR6ijo6NdcrIu4iVnnDpzjhHfpzD8uxQKh4fyQsca3FarDGbm9Wgil8TMljjnMr2lrSdFJSCEhQQxoGU1vnqyBRVKFKDfp8vo+UEyPx3ST9mK/1CgS0C57urCfP54U/7c4XrmpOyjVXwiny5S2Zf4BwW6BJzgIOPRFtcyY0AMNcsV5bnPV3HPmIVs23/U69FErogCXQJWxasK8knPRrza5QZW70wr+xqTtEVlX5JnKdAloJkZ3RpeQ0JcLM2rlORvU9fRZcRcNvyksi/JexToIsDVRcMZ80A0w7rXZcfB49w6bDaDEjaq7EvyFAW6SDoz47baZUmIi6XDDWUY8s0mbh02m+Uq+5I8QoEucp4SBcMY3K0u7zwYzS8nztBlxFxe+Wqtyr4k11Ogi1zAzdVLM3NgDPc0uoa35vxAm8FJzEvZ5/VYIhekQBf5HYXDQ3ml8w2M7dWYIIN73lrIsxNXcui4yr4k91Ggi2RB42uvYvqAGB6LvZbxydtpPSiRhLUq+5LcRYEukkXhocE81+56JvVpRvECYfT8IJm+nyxln8q+JJdQoItcolrlizGlb3OealWNmWv20Co+kUnLdqo+QDynQBe5DGEhQTx5S1W+7tecyJIFGTBuOY+8n8yun497PZoEMAW6yBWoWrowE3o35flbo5i/eT+tByXx0YJtnFN9gHhAgS5yhYKDjIebV2LmwBjqVCjGnyetptuYBfywT2VfkrMU6CI+UqFEAT58pCGv3VGL9bsP03ZwEiMTN3PmrOoDJGco0EV8yMy4q0EFEuJiia0WwavT1nP7iHms3XXY69EkACjQRbJB6SLhjLq/PsPvqcfuQ8fp+MYc/j1zAyfPqD5Aso8CXSSbmBkdapUhYWAsHeuUZdi3KXQYOocl2w56PZr4KQW6SDYrXjCM+Lvq8N5DDTh+6ixdR87jxS/XcPTkGa9HEz+jQBfJITdeV4oZA2O4v3FF3p27lTaDk5i9KdXrscSPKNBFclChfCG81Kkm4x9rQlhwEPe/vYinJ6zg0DGVfcmVU6CLeKBhpRJM7d+Cx2+szMSlO2k5KJHpq3/yeizJ4xToIh4JDw3mmbbVmdynGRGF8tH7oyX0+Xgpqb+o7EsujwJdxGM1yxVlct9m/LHNdSSs3UPL+EQmLtmhsi+5ZAp0kVwgNDiIPjdVYWr/FlQpVYinPltBj3cXs+PgMa9HkzxEgS6Si1QpVYjPHmvCix1rkLz1AG0GJfHB/K0q+5IsUaCL5DJBQUaPppHMGBBDvYrFeX7yGu4ePZ/NqUe8Hk1yuSwFupm1NbMNZpZiZs9m8npvM1tlZsvNbI6ZRfl+VJHAUqFEAT54uCH/urM2G/ccod2Q2Yz4PoXTKvuSC7hooJtZMDAcaAdEAd0zCexPnHM3OOfqAK8B8b4eVCQQmRld65cnIS6GW6qX4rXpG+g8fC6rdx7yejTJhbJyhd4QSHHObXHOnQLGAp0yHuCcy1glVxDQDT8RHypVOJw376vPm/fWY8/hk3QaPpfXpq/nxGmVfcl/ZSXQywHbM2zvSN/3G2bWx8w2k3aF3i+zb2Rmvcws2cySU1P1yLPIpWp3QxlmxcVwe91yjPh+M+2HziZ56wGvx5JcwmcfijrnhjvnKgPPAH++wDGjnXPRzrnoiIgIX51aJKAUKxDGv+6szQcPN+Tk6XPcOWo+f528miMq+wp4WQn0nUCFDNvl0/ddyFig8xXMJCJZEFMtgpkDY+jRJJIPFmyjzaAkEjfq//kGsqwE+mKgqplVMrMwoBswJeMBZlY1w2YHYJPvRhSRCymYL4QXOtbgs8eaEB4aRI93FhE3fjk/Hzvl9WjigYsGunPuDNAXmAGsA8Y759aY2Utm1jH9sL5mtsbMlgNxQI/sGlhE/ld0ZAm+7teCvjdVYfLyXbSMT2Tqqt1ejyU5zLzqi4iOjnbJycmenFvEn63ZdYinJ6xkza7DtKlRmpc71aRUkXCvxxIfMbMlzrnozF7Tk6IifqZG2aJM7tOMp9tex3cbUmkZn8j45O0q+woACnQRPxQSHMQTN1ZhWv8WXHd1YZ6esJIH3lnE9gMq+/JnCnQRP1Y5ohDjejXh5U41WLrtIK0HJfHu3B84q7Ivv6RAF/FzQUHG/U0imRkXS8NKJXjxy7XcOXIeKXt/8Xo08TEFukiAKFcsP+891ID4u2qzZd9R2g+ZwxvfblLZlx9RoIsEEDOjS73yJAyMpVWN0vxr5kZuGzaHVTtU9uUPFOgiASiicD6G31OPUffXZ//RU3QeMZdXp6nsK69ToIsEsDY1rmbWwFi61ivPyMTNtBsym4Vb9ns9llwmBbpIgCtaIJR/dq3Fx4824sy5c9w9egF/nrSKX06c9no0uUQKdBEBoFmVkswYEMPDzSrx8cIfaTMoie/W7/V6LLkECnQR+Y8CYSE8f1sUEx9vSsF8ITz03mIGjlvOgaMq+8oLFOgi8j/qXVOcr/o1p98tVflyxS5axSfy1cpdqg/I5RToIpKpfCHBxLWqxpdPNqdc8fz0/WQZvT5cwp7DJ7weTS5AgS4iv+v6MkX4/PGm/F/76iRtTCv7GrvoR12t50IKdBG5qJDgIHrFVGbGgBiiyhTh2c9Xce9bC/lxv8q+chMFuohkWWTJgnzaszF/u70mK3ccovXgRN6avUVlX7mEAl1ELklQkHFvo4okxMXQtHJJXvl6HXe8OY+Ne1T25TUFuohcljJF8/N2j2iGdKvDjweO0WHobIbM2sSpMyr78ooCXUQum5nRqU45EgbG0K5mGQbN2kjHN+awYvvPXo8WkBToInLFriqUj6Hd6/LWA9H8fOw0t4+Yy9+nruP4KZV95SQFuoj4TMuo0syMi6Fbw2sYnbSFtkOSmL9ZZV85RYEuIj5VJDyUv99+A5/0bARA9zELeO7zVRxW2Ve2U6CLSLZoWrkk0/vH0LNFJcYt/pHW8Ul8s26P12P5NQW6iGSb/GHB/KlDFF880YxiBUJ55P1k+n26jP1HTno9ml9SoItItqtdoRhT+jZnYMtqTFu9m1aDkpi8fKfqA3xMgS4iOSIsJIj+Lavydb8WXFOiAP3HLufR95PZfei416P5DQW6iOSoaqULM/Hxpvy5w/XM3byPVvFJfLxwG+dUH3DFFOgikuOCg4xHW1zLzAGx1CpflD99sZp73lrA1n1HvR4tT1Ogi4hnrrmqAB8/2ohXu9zAmp2HaTM4idFJmzlzVvUBlyNLgW5mbc1sg5mlmNmzmbweZ2ZrzWylmX1jZhV9P6qI+CMzo1vDa0iIi6VF1Qj+PnU9d7w5j/U/HfZ6tDznooFuZsHAcKAdEAV0N7Oo8w5bBkQ752oBE4DXfD2oiPi3q4uGM+aB+gzrXpcdB49z69A5xCds5OQZ1QdkVVau0BsCKc65Lc65U8BYoFPGA5xz3znnfm26XwCU9+2YIhIIzIzbapclIS6W22qXZeg3m7ht2ByW/XjQ69HyhKwEejlge4btHen7LuQRYFpmL5hZLzNLNrPk1NTUrE8pIgGlRMEwBt1dh3cfbMAvJ87Q5c15vPzVWo6dOuP1aLmaTz8UNbP7gGjg9cxed86Nds5FO+eiIyIifHlqEfFDN1UvxcyBMdzb6BrenvMDbQfPZl7KPq/HyrWyEug7gQoZtsun7/sNM2sJ/Ano6JzTc70i4hOFw0N5pfMNjOvVmOAg4563FvLsxJUcOq6yr/NlJdAXA1XNrJKZhQHdgCkZDzCzusAo0sJ8r+/HFJFA1+jaq5jWvwWPxV7L+OTttIpPZOaan7weK1e5aKA7584AfYEZwDpgvHNujZm9ZGYd0w97HSgEfGZmy81sygW+nYjIZQsPDea5dtczqU8zShQMo9eHS+j7yVL2qewLAPOqHCc6OtolJyd7cm4RyftOnz3HyO83M+zbFArkC+avt0XRuU45zMzr0bKVmS1xzkVn9pqeFBWRPCk0OIgnb6nK1/2aU6lkQQaOW8HD7y1m18+BW/alQBeRPK1q6cJM6N2Uv94WxYItB2g9KIkPFwRm2ZcCXUTyvOAg46FmlZg5MIY6FYrxl0mr6TZmAT8EWNmXAl1E/EaFEgX48JGGvNa1Fut3H6bt4CRGJgZO2ZcCXUT8iplxV3QFZsXFcuN1Ebw6bT2dR8xl7S7/L/tSoIuIXypVJJyR99VnxL31+OnQCTq+MYd/z9zg12VfCnQR8VtmRvsbypAwMJaOdcoy7NsUOgydw5Jt/ln2pUAXEb9XvGAY8XfV4b2HGnD81Fm6jpzHi1+u4ehJ/yr7UqCLSMC48bpSzBgYwwONK/Lu3K20GZzE7E3+0/yqQBeRgFIoXwgvdqrJZ72bEBYSxP1vL+KPn63g0LG8X/alQBeRgNQgsgRT+7XgiRsr8/mynbQclMj01Xm77EuBLiIBKzw0mKfbVmdyn2ZEFMpH74+W8MTHS9j7ywmvR7ssCnQRCXg1yxVlct9m/LHNdcxat5dW8UlMXLIDr8oLL5cCXUSEtLKvPjdVYWq/FlQtVYinPltBj3cXs+PgsYv/w7mEAl1EJIMqpQox/rEmvNixBslb08q+3p+3NU+UfSnQRUTOExRk9GgaycyBMURHluCvU9Zw16j5bE494vVov0uBLiJyAeWLF+D9hxrwrztrs2nvEdoNmc3w71I4nUvLvhToIiK/w8zoWr88CXExtLy+FK/P2EDn4XNZvfOQ16P9DwW6iEgWlCoczoh76zPyvnrs/eUknYbP5bXp6zlxOveUfSnQRUQuQduaZZg1MJYudcsx4vvNtB8ym8VbD3g9FqBAFxG5ZEULhPL6nbX58JGGnDp7jjtHzuf5yas54nHZlwJdROQytagawYwBMTzYNJIPF2yjzaAkEjd6V/alQBcRuQIF84XwQscaTOjdhPDQIHq8s4i48cs5ePRUjs+iQBcR8YH6FUswtX8Lnry5ClOW76LVoESmrtqdo/UBCnQRER/JFxLMU62vY0rf5pQpmp8nPl5K74+WsPdwzpR9KdBFRHwsqmwRvniiKc+2q873G1JpGZ/I+OTt2X61rkAXEckGIcFB9I6tzLT+LahepghPT1jJ/W8vYvuB7Cv7UqCLiGSjayMKMbZnY17pXJPl23+m9aAkvlyxK1vOlaVAN7O2ZrbBzFLM7NlMXo8xs6VmdsbMuvp+TBGRvCsoyLivcUVmDoyhWZWSVCpZMFvOE3KxA8wsGBgOtAJ2AIvNbIpzbm2Gw34EHgT+kB1Dioj4g7LF8vNWj+hs+/4XDXSgIZDinNsCYGZjgU7AfwLdObc1/bXcWUEmIhIAsnLLpRywPcP2jvR9l8zMeplZspklp6Z69zSViIg/ytEPRZ1zo51z0c656IiIiJw8tYiI38tKoO8EKmTYLp++T0REcpGsBPpioKqZVTKzMKAbMCV7xxIRkUt10UB3zp0B+gIzgHXAeOfcGjN7ycw6AphZAzPbAdwJjDKzNdk5tIiI/K+s/JQLzrmpwNTz9j2f4evFpN2KERERj+hJURERP2E5We34mxObpQLb0jdLAvs8GcR7WnvgCuT1B/La4crWX9E5l+mPCXoW6L8ZwizZOZd9j0/lYlp7YK4dAnv9gbx2yL7165aLiIifUKCLiPiJ3BLoo70ewENae+AK5PUH8tohm9afK+6hi4jIlcstV+giInKFFOgiIn7C00C/2G9C8jdm9o6Z7TWz1Rn2lTCzBDPblP53cS9nzC5mVsHMvjOztWa2xsz6p+/3+/WbWbiZLTKzFelrfzF9fyUzW5j+/h+X3pXkl8ws2MyWmdlX6duBtPatZrbKzJabWXL6vmx533sW6Bl+E1I7IArobmZRXs2TQ94D2p6371ngG+dcVeCb9G1/dAZ4yjkXBTQG+qT/7x0I6z8J3Oycqw3UAdqaWWPgn8Ag51wV4CDwiHcjZrv+pHVB/SqQ1g5wk3OuToafPc+W972XV+j/+U1IzrlTwK+/CclvOeeSgAPn7e4EvJ/+9ftA55ycKac453Y755amf/0Laf9ylyMA1u/SHEnfDE3/44CbgQnp+/1y7QBmVh7oALyVvm0EyNp/R7a8770MdJ/9JqQ8rrRzbnf61z8Bpb0cJieYWSRQF1hIgKw//ZbDcmAvkABsBn5ObzMF/37/DwaeBn79FZVXEThrh7T/eM80syVm1it9X7a877PUtig5wznnzMyvf47UzAoBE4EBzrnDaRdrafx5/c65s0AdMysGfAFU93ainGFmtwJ7nXNLzOxGj8fxSnPn3E4zKwUkmNn6jC/68n3v5RW6fhNSmj1mVgYg/e+9Hs+TbcwslLQw/9g593n67oBZP4Bz7mfgO6AJUMzMfr2o8tf3fzOgo5ltJe226s3AEAJj7QA453am/72XtP+YNySb3vdeBrp+E1KaKUCP9K97AJM9nCXbpN83fRtY55yLz/CS36/fzCLSr8wxs/xAK9I+Q/gO6Jp+mF+u3Tn3nHOuvHMukrR/x791zt1LAKwdwMwKmlnhX78GWgOryab3vadPippZe9LurwUD7zjn/ubZMDnAzD4FbiStOnMP8FdgEjAeuIa0OuG7nHPnf3Ca55lZc2A2sIr/3kv9P9Luo/v1+s2sFmkffAWTdhE13jn3kpldS9pVawlgGXCfc+6kd5Nmr/RbLn9wzt0aKGtPX+cX6ZshwCfOub+Z2VVkw/tej/6LiPgJPSkqIuInFOgiIn5CgS4i4icU6CIifkKBLiLiJxToIiJ+QoEuIuIn/h9/yXz1e4XJEAAAAABJRU5ErkJggg==",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "atp = np.linspace(2, 50, 10)\n",
    "biomass = []\n",
    "for i in atp:\n",
    "    m_fc.reactions.get_by_id(\"ATPase\").bounds = (i, 1000)\n",
    "    biomass.append(pfba(m_fc).to_frame()[\"fluxes\"][\"BIOMASS\"])\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "ax.plot(atp, biomass)\n",
    "plt.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## SBML export\n",
    "\n",
    "To export a model SBML including all desired annotations we use the `to_sbml` function to create a sbml model which can be re-used and tested using MEMOTE."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "fastacyc.to_sbml(Path(\"..\") / \"generated\" / \"models\" / \"bacillus.xml\")\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "interpreter": {
   "hash": "5d2862dc9757a2bae5a59ffe521fdff6fdfe254fa3ab6eceec15fc2a32be6608"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
