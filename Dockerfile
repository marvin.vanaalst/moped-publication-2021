FROM jupyter/minimal-notebook:notebook-6.4.6

WORKDIR /home/jovyan

RUN wget -O ncbi-blast.tar.gz https://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.12.0/ncbi-blast-2.12.0+-x64-linux.tar.gz \
    && tar -xzvf ncbi-blast.tar.gz \
    && rm ncbi-blast.tar.gz 

ENV PATH="/home/jovyan/ncbi-blast-2.12.0+/bin:${PATH}"

COPY ./data ./data
COPY ./figures ./figures
COPY ./generated ./generated
COPY ./moped2021 ./moped2021
COPY ./temp ./temp
COPY ./pyproject.toml ./pyproject.toml
COPY ./poetry.lock ./poetry.lock
COPY ./README.md ./README.md
RUN pip install .

WORKDIR /home/jovyan/moped2021


